/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author AuyouknoW
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"chayen",30);
        Product p2 = new Product(1,"americano",40);
        User seller= new User("Naded","88888888","password");
        Customer customer = new Customer(1,"Prayut","9999999");
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        System.out.println(receipt);
        receipt.deleteReceiveDetail(0);
        System.out.println(receipt);
    }
}
