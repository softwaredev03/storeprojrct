/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.teerarak.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
/**
 *
 * @author AuyouknoW
 */
public class CustomerDAO implements daoInterface<Customer>{

    @Override
    public int add(Customer object) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql ="INSERT INTO customer (id,name,tel ) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1,object.getId());
            stmt.setString(2,object.getName());
            stmt.setString(3, object.getTel());
            int row = stmt.executeUpdate();
            System.out.println("Affect row"+row);
            ResultSet result = stmt.getGeneratedKeys();

            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return id;
    }
    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
           
                
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(5,name,tel);
                System.out.println(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM customer WHERE id = "+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
           
                
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(id,name,tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }

    @Override
    public int update(Customer object) {
      Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(2, object.getName());
            stmt.setString(3, object.getTel());
            stmt.setInt(1, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row" + row );

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }
    public static void main(String[] args) {
        CustomerDAO dao = new CustomerDAO();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(4,"natacha","0552224433"));
        System.out.println("id :"+ id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last Customer: "+ lastCustomer);
        lastCustomer.setTel("0992135555");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: "+updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer: "+deleteCustomer);
    }
    
    
}
