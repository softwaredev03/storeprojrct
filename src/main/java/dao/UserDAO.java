/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.teerarak.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.User;
/**
 *
 * @author AuyouknoW
 */
public class UserDAO implements daoInterface<User>{

    @Override
    public int add(User object) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql ="INSERT INTO user (id,name,tel,password ) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1,object.getId());
            stmt.setString(2,object.getName());
            stmt.setString(3, object.getTel());
            stmt.setString(4,object.getPassword());
            int row = stmt.executeUpdate();
            System.out.println("Affect row"+row);
            ResultSet result = stmt.getGeneratedKeys();

            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
           
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(name,tel,password);
                System.out.println(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return list;
    }
    @Override
    public User get(int id) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM user WHERE id = "+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
           
                
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(name,tel,password);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
       Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,tel = ?,password = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(2, object.getName());
            stmt.setString(3, object.getTel());
            stmt.setString(4,object.getPassword());
            stmt.setInt(1, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row" + row );

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }
    public static void main(String[] args) {
        UserDAO dao = new UserDAO();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User("captainAmerica","0552224433","password1"));
        System.out.println("id :"+ id);
        User lastUser = dao.get(id);
        System.out.println("last User: "+ lastUser);
        lastUser.setPassword("passaway");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("update customer: "+updateUser);
        dao.delete(id);
        User deleteUser = dao.get(id);
        System.out.println("delete user: "+deleteUser);
    }
    
}
