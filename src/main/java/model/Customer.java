/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author AuyouknoW
 */
public class Customer {
    private int id;
    private String Name;
    private String tel;

    public Customer(int id,String Name, String tel) {
        this.id = id;
        this.Name = Name;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "customer{" + "id=" + id + ", Name=" + Name + ", tel=" + tel + '}';
    }
    
}
