/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author AuyouknoW
 */
public class User {
    private int id;
    private String Name;
    private String tel;
    private String password;

    public User(int id ,String Name, String tel, String password) {
        this.id = id;
        this.Name = Name;
        this.tel = tel;
        this.password = password;
    }
    public User(String name, String tel,String password) {
        this(-1,name,tel,password);
    }
    public User(String name, String tel) {
        this(-1,name,tel,"");
    }
    public User(int id,String name, String tel) {
        this(id,name,tel,"");
    }
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "user{" + "id=" + id + ", Name=" + Name + ", tel=" + tel + ", password=" + password + '}';
    }
    
}
